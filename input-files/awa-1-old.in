/*
	AWA Gun, configured in this input file for fast execution 
	(low particle number & low fieldsolver resolution, large
	timestep and only one energy bin).
*/

OPTION, PSDUMPFREQ      = 10;    // 6d data written every 10th time step (h5).
OPTION, STATDUMPFREQ    = 1;     // Beam Stats written every time step (stat).
OPTION, BOUNDPDESTROYFQ = 10;    // Delete lost particles, if any out of 10 \sigma
OPTION, AUTOPHASE       = 4;     // Autophase is on, and phase of max energy
                                 // gain will be found automatically for cavities.
OPTION, VERSION=10900;

Title, string="AWA Photoinjector";

Value,{OPALVERSION};

// ----------------------------------------------------------------------------
// Global Parameters

REAL rf_freq             = 1.3e9;    // RF frequency. (Hz)
REAL n_particles         = 1E4;      // Number of particles in simulation.
REAL beam_bunch_charge   = 1e-9;     // Charge of bunch. (C)

//----------------------------------------------------------------------------
//Fieldsolver configuration
REAL MX   = 16;
REAL MY   = 16;
REAL MZ   = 16;

//----------------------------------------------------------------------------
//Initial Momentum Calculation
REAL Edes    = 1.0; //initial energy in GeV
REAL gamma   = (Edes+EMASS)/EMASS; 
REAL beta    = sqrt(1-(1/gamma^2));
REAL P0      = gamma*beta*EMASS;    //inital z momentum

//Printing initial energy and momentum to terminal output.
value, {Edes, P0};

D1: DRIFT, L = 1., ELEMEDGE = 0.0;
D2: DRIFT, L = 1., ELEMEDGE = 1.;
D3: DRIFT, L = 1., ELEMEDGE = 2.;
D4: DRIFT, L = 1., ELEMEDGE = 3.;

myLine: Line = (D1,D2,D3,D4);

BEAM1:  BEAM, PARTICLE = ELECTRON, pc = P0, NPART = n_particles,
        BFREQ = rf_freq, BCURRENT = beam_bunch_charge * rf_freq * 1E6, CHARGE = -1;


FS1: Fieldsolver, FSTYPE = NONE, MX = MX, MY = MY, MT = 64, 
            PARFFTX = true, PARFFTY = true, PARFFTT = true,
            BCFFTX = open, BCFFTY = open, BCFFTT = open,
            BBOXINCR = 1, GREENSF = INTEGRATED;

Dist1: DISTRIBUTION, TYPE= GAUSS, SIGMAX=1e-3, SIGMAY=1e-3;


TRACK, LINE = myLine, BEAM = BEAM1, MAXSTEPS = 1, DT = {1e-12}, ZSTOP={7.0}; 
 RUN, METHOD = "PARALLEL-T", BEAM = BEAM1, FIELDSOLVER = FS1, DISTRIBUTION = Dist1;
ENDTRACK;

QUIT;