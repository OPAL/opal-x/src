/*      Drift-0.in
	OPALX
	Simple Gaussian distribution test, zero steps
*/

OPTION, PSDUMPFREQ      = 10;    // 6d data written every 10th time step (h5).
OPTION, STATDUMPFREQ    = 1;     // Beam Stats written every time step (stat).
OPTION, BOUNDPDESTROYFQ = 10;    // Delete lost particles, if any out of 10 \sigma
OPTION, AUTOPHASE       = 4;     // Autophase is on, and phase of max energy
                                 // gain will be found automatically for cavities.
OPTION, VERSION=10900;

Title, string="Test simple Gaussian distribution with space charge";

Value,{OPALVERSION};

// ----------------------------------------------------------------------------
// Global Parameters

REAL rf_freq             = 1.3e9;    // RF frequency. (Hz)
REAL n_particles         = 1E5;      // Number of particles in simulation.
REAL beam_bunch_charge   = 1e-9;     // Charge of bunch. (C)

// ----------------------------------------------------------------------------
// Initial Momentum Calculation

REAL Edes    = 0.01; //initial energy in GeV
REAL gamma   = (Edes+EMASS)/EMASS; 
REAL beta    = sqrt(1-(1/gamma^2));
REAL P0      = gamma*beta*EMASS;    //inital z momentum

//Printing initial energy and momentum to terminal output.
value, {Edes, P0};

D1: DRIFT, L = 1.0, ELEMEDGE = 0.0;
myLine: Line = (D1);

BEAM1:  BEAM, PARTICLE = ELECTRON, pc = P0, NPART = n_particles,
        BFREQ = rf_freq, BCURRENT = beam_bunch_charge * rf_freq, CHARGE = -1;


FS1: Fieldsolver, NX=32, NY=32, NZ=32, TYPE=NONE, PARFFTX = true, PARFFTY = true, PARFFTZ = false,
                  BCFFTX=OPEN, BCFFTY=OPEN, BCFFTZ=OPEN,
                  BBOXINCR = 5, GREENSF = STANDARD;

Dist1: DISTRIBUTION, TYPE= GAUSS, SIGMAX=1e-3, SIGMAY=5e-3, SIGMAZ=10e-3, SIGMAPX=1e-6, SIGMAPY=1e-6, SIGMAPZ=1e-6;


TRACK, LINE = myLine, BEAM = BEAM1, MAXSTEPS = 1, DT = {5e-11}, ZSTOP=1.0; 
 RUN, METHOD = "PARALLEL", BEAM = BEAM1, FIELDSOLVER = FS1, DISTRIBUTION = Dist1;ENDTRACK;
QUIT;
