/*! \mainpage
<P>
<B>
OPALX is a parallel open source tool for charged-particle optics in linear
accelerators and rings, including 3D space charge. Using the MAD language
with extensions, OPALX can run on a laptop as well as on the largest high
performance computing systems. OPALX is built from the ground up as a
parallel application, is perormance portable and exascale ready. 

The OPALX framework makes it easy to add new features in the form of new
C++ classes.
</B>
</P>

<P>

<P>
For further information please contact us on the <a href="mailto:opal@lists.psi.ch">OPAL mailing list</a>
</P>
</P>

<P>
<a href="http://www.psi.ch">PSI Disclaimers</a><br>
</P>
*/
