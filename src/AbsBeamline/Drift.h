#ifndef CLASSIC_Drift_HH
#define CLASSIC_Drift_HH

// ------------------------------------------------------------------------
// $RCSfile: Drift.h,v $
// ------------------------------------------------------------------------
// $Revision: 1.1.1.1 $
// ------------------------------------------------------------------------
// Copyright: see Copyright.readme
// ------------------------------------------------------------------------
//
// Class: Drift
//   Defines the abstract interface for a drift space.
//
// ------------------------------------------------------------------------
// Class category: AbsBeamline
// ------------------------------------------------------------------------
//
// $Date: 2000/03/27 09:32:31 $
// $Author: fci $
//
// ------------------------------------------------------------------------

#include "AbsBeamline/Component.h"

#ifdef __CUDACC__
#pragma push_macro("__cpp_consteval")
#pragma push_macro("_NODISCARD")
#pragma push_macro("__builtin_LINE")

#define __cpp_consteval 201811L

#ifdef _NODISCARD
    #undef _NODISCARD
    #define _NODISCARD
#endif

#define consteval constexpr

#include <source_location>

#undef consteval
#pragma pop_macro("__cpp_consteval")
#pragma pop_macro("_NODISCARD")
#else
#include <source_location>
#endif

// Class Drift
// ------------------------------------------------------------------------
/// Interface for drift space.
//  Class Drift defines the abstract interface for a drift space.

class Drift : public Component {
public:
    /// Constructor with given name.
    explicit Drift(const std::string& name);

    Drift();
    Drift(const Drift& right);
    virtual ~Drift();

    /// Apply visitor to Drift.
    virtual void accept(BeamlineVisitor&) const override;

    virtual void initialise(PartBunch_t* bunch, double& startField, double& endField) override;

    virtual void finalise() override;

    virtual bool bends() const override;

    virtual ElementType getType() const override;

    virtual void getDimensions(double& zBegin, double& zEnd) const override;

    // set number of slices for map tracking
    void setNSlices(const std::size_t& nSlices);  // Philippe was here

    // set number of slices for map tracking
    std::size_t getNSlices() const;  // Philippe was here

    virtual int getRequiredNumberOfTimeSteps() const override;

private:
    double startField_m;
    std::size_t nSlices_m;

    // Not implemented.
    void operator=(const Drift&);
};

inline int Drift::getRequiredNumberOfTimeSteps() const {
    return 1;
}

#endif  // CLASSIC_Drift_HH
