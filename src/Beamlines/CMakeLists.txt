set (_SRCS
    Beamline.cpp
    BeamlineGeometry.cpp
    ElmPtr.cpp
    FlaggedBeamline.cpp
    FlaggedElmPtr.cpp
    )

include_directories (
    ${CMAKE_CURRENT_SOURCE_DIR}
    )

add_opal_sources (${_SRCS})

set (HDRS
    BeamlineGeometry.h
    Beamline.h
    ElmPtr.h
    FlaggedBeamline.h
    FlaggedElmPtr.h
    TBeamline.h
    )

install (FILES ${HDRS} DESTINATION "${CMAKE_INSTALL_PREFIX}/include/Beamlines")
